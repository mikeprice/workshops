# -*- encoding: utf-8 -*-

from classifier import ToShootViolentContextSpanishTextClassifier

c = ToShootViolentContextSpanishTextClassifier()

c.load('feature_names.pkl', 'model.pkl')

examples = [
    "Adulto mayor se suicida con un disparo en su cabeza.#Cauquenes._ "
    "Alrededor de las 02:50 horas de este domingo 2",
    "GOL de Liam Sercombe para el Oxford Utd. Disparo raso pegado al palo "
    "tras salida de córner. 77 Coventry 2-1 Oxford Utd. #CT"
]

prediction = c.predict(examples)

print(prediction)
