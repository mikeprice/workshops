import codecs
import joblib
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_predict


class ToShootViolentContextSpanishTextClassifier(object):
    @property
    def dataset(self):
        return self._dataset

    @dataset.setter
    def dataset(self, value):
        self._dataset = value

    def extract_features(self):
        self._count_vectorizer = CountVectorizer(analyzer='word')
        self._X = self._count_vectorizer.fit_transform(
                      self._dataset['example'])
        self._feature_names = self._count_vectorizer.get_feature_names()

    def accuracy(self):
        return accuracy_score(self._dataset['label'],
                              cross_val_predict(LogisticRegression(),
                                                self._X,
                                                self._dataset['label']))

    def train(self):
        self._model = LogisticRegression()
        self._model.fit(self._X, self._dataset['labels'])

    def dump(self, path_feature_names_pkl, path_model_pkl):
        joblib.dump(self._feature_names, path_feature_names_pkl)
        joblib.dump(self._model, path_model_pkl)

    def debug(self):
        return sorted([(word, self._X.getcol(idx).sum()) for word, idx in
                      self._count_vectorizer.vocabulary_.items()],
                      key=lambda x: -x[1])

    def load(self, path_feature_names_pkl, path_model_pkl):
        self._feature_names = joblib.load(path_feature_names_pkl)
        self._model = joblib.load(path_model_pkl)
        self._count_vectorizer = CountVectorizer(
                                      analyzer='word',
                                      vocabulary=self._feature_names)

    def predict(self, examples):
        return self._model.predict(self._count_vectorizer.transform(examples))
