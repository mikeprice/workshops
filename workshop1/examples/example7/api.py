from classifier import ToShootViolentContextSpanishTextClassifier
from flask import Flask
from flask_restful import reqparse, Resource, Api

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('text')

classifier = ToShootViolentContextSpanishTextClassifier()

classifier.load('feature_names.pkl', 'model.pkl')


class Predict(Resource):
    def post(self):
        args = parser.parse_args()

        prediction = True if classifier.predict([args['text']])[0] == 'TRUE' \
            else False

        return {'result': prediction}

api.add_resource(Predict, '/predict')

if __name__ == '__main__':
    app.run(debug=False)
