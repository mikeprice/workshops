import codecs
import os
from time import sleep
from twitter import Twitter, OAuth

COUNT = 100
QUERY = 'disparar OR dispara OR disparo OR dispare -RT'

t = Twitter(auth=OAuth(os.environ.get('ACCESS_TOKEN'),
                       os.environ.get('ACCESS_TOKEN_SECRET'),
                       os.environ.get('CONSUMER_KEY'),
                       os.environ.get('CONSUMER_SECRET')))

tweets = t.search.tweets(q=QUERY, count=COUNT)

values = [['ID', 'label', 'example']]

while True:
    id = None

    for tweet in tweets['statuses']:
        text = tweet['text']

        for c in ['\n', ',', '"', '\'']:
            text = text.replace(c, '')

        values.append([tweet['id'], 'undefined', text])

        if not id or tweet['id'] < id:
            id = tweet['id']

    if len(tweets['statuses']) < COUNT:
        break

    sleep(1)

    tweets = t.search.tweets(q=QUERY, count=COUNT, max_id=id)

file = codecs.open('dataset-0.csv', 'w', 'utf-8')

for value in values:
    file.write(u'{0},{1},{2}\n'.format(value[0], value[1], value[2]))

file.close()
