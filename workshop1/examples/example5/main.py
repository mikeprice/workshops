from classifier import ToShootViolentContextSpanishTextClassifier
import pandas

c = ToShootViolentContextSpanishTextClassifier()

c.dataset = pandas.read_csv('dataset-1.csv', encoding='utf-8',
                            dtype={'label': object, 'example': object})

c.extract_features()

accuracy = c.accuracy()

print(accuracy)

c.train()

c.dump('feature_names.pkl', 'model.pkl')
