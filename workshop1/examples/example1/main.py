import os
from twitter import Twitter, OAuth

t = Twitter(auth=OAuth(os.environ.get('ACCESS_TOKEN'),
                       os.environ.get('ACCESS_TOKEN_SECRET'),
                       os.environ.get('CONSUMER_KEY'),
                       os.environ.get('CONSUMER_SECRET')))

tweets = t.search.tweets(q='Pugg')

for tweet in tweets['statuses']:
    print(tweet['text'])
